window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();

        for (let conference of data.conferences) {
            const option = document.createElement('option');
            option.value = conference.href;
            option.innerHTML = conference.name;
            selectTag.appendChild(option);
        }

        const loadingIcon = document.getElementById('loading-conference-spinner')
        loadingIcon.classList.add("d-none")

        const showConferenceOptions = document.getElementById("conference")
        showConferenceOptions.classList.remove("d-none")

        const formTag = document.getElementById('create-attendee-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            console.log('need to submit the form data');
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            const attendeeObject = JSON.parse(json)
            console.log(attendeeObject)
            //${attendeeObject.conference} = /api/conference/1/

            const attendeeUrl = `http://localhost:8001${attendeeObject.conference}attendees/`;
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const attendeeResponse = await fetch(attendeeUrl, fetchConfig);
            if (attendeeResponse.ok) {
                formTag.reset();
                const newattendee = await attendeeResponse.json();
                console.log(newattendee);
            }
            const attendeeForm = document.getElementById('create-attendee-form')
            attendeeForm.classList.add("d-none")

            const successMessage = document.getElementById("success-message")
            successMessage.classList.remove("d-none")
        });

    }

  });
