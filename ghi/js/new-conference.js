window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/locations/';
    let response = await fetch(url);
    if (response.ok) {
        let data = await response.json();
        console.log(data)
        const selectTag = document.getElementById('location');
        for (let location of data.locations) {
            console.log(location)
            let option = document.createElement('option');
            option.value = location.id;
            option.innerHTML = location.name;
            selectTag.appendChild(option);
        }

        const formTag = document.getElementById('create-conference-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            console.log('need to submit the form data');
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            console.log(json);

            const conferenceUrl = 'http://localhost:8000/api/conferences/';
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const conferenceResponse = await fetch(conferenceUrl, fetchConfig);
            if (conferenceResponse.ok) {
                formTag.reset();
                const newConference = await conferenceResponse.json();
                console.log(newConference);
            }
        });
    }
});
