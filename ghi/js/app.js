function createCard(name, description, pictureUrl,starts, ends, loc_name) {
  return `
    <div className="col">
      <div className="card shadow mb-4 bg-body-tertiary rounded">
        <img src="${pictureUrl}" className="card-img-top">
        <div className="card-body">
          <h5 className="card-title">${name}</h5>
          <h6 className="card-subtitle mb-2 text-muted">${loc_name}</h6>
          <p className="card-text ">${description}</p>
        </div>
        <div className="card-footer">${starts} - ${ends}</div>
      </div>
    </div>
  `;
}

function placeholder() {
  return `<div className="card" id="placeholder" aria-hidden="true">
  <div className="card-body">
    <h5 className="card-title placeholder-glow">
      <span className="placeholder col-6"></span>
    </h5>
    <p className="card-text placeholder-glow">
      <span className="placeholder col-7"></span>
      <span className="placeholder col-4"></span>
      <span className="placeholder col-4"></span>
      <span className="placeholder col-6"></span>
      <span className="placeholder col-8"></span>
    </p>
    <a href="#" tabindex="-1" className="btn btn-primary disabled placeholder col-6"></a>
  </div>
</div>`
}

function createError() {
return `
  <div className="alert alert-danger" role="alert">
  A simple danger alert—check it out!
  </div>
`;
}

window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      const bodyTag = document.querySelector('body');
      const error = createError();
      bodyTag.innerHTML += error;

    } else {
      const data = await response.json();
      for (let x = 0; x < data.conferences.length; x++) {
        const column = document.querySelector('#rows');
        column.innerHTML += placeholder()

      }
      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const name = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const start_date = new Date(details.conference.starts)
          const starts = `${start_date.getMonth()+1}/${start_date.getDate()}/${start_date.getFullYear()}`
          const end_date = new Date(details.conference.ends)
          const ends = `${end_date.getMonth()+1}/${end_date.getDate()}/${end_date.getFullYear()}`
          const loc_name = details.conference.location.name;

          const column = document.querySelector('#rows');
          const html = createCard(name, description, pictureUrl, starts, ends, loc_name);

          var placeHolder = document.querySelector('#placeholder')
          placeHolder.remove()

          column.innerHTML += html;

          console.log(details);

        }
      }
    }
  } catch(err) {
    const bodyTag = document.querySelector('body');
    const error = createError();
    bodyTag.innerHTML += error;
  }

});
