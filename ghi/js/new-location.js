window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/states/';
    let response = await fetch(url);
    if (response.ok) {
        let data = await response.json();

        const selectTag = document.getElementById('state');
        for (let state of data.states) {

            let option = document.createElement('option');
            option.value = state.abbreviation;
            option.innerHTML = state.name;
            selectTag.appendChild(option);
        }

        const formTag = document.getElementById('create-location-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            console.log('need to submit the form data');
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            console.log(json);

            const locationUrl = 'http://localhost:8000/api/locations/';
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const locationResponse = await fetch(locationUrl, fetchConfig);
            if (locationResponse.ok) {
                formTag.reset();
                const newLocation = await locationResponse.json();
                console.log(newLocation);
            }
        });
    }


});
